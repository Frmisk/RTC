WebRTC trial
============
WebRTC on browser with simple signalling

1. run `node backend.js` in ./, it is web front end server and web socket server for signalling
2. open two browsers with the same URL, http://localhost:8000/.
3. press **Start video** button in both browsers
4. press **Connect** button in one browser
5. SDP and ICE candidates were exchanged via web socket and web rtc started!!
